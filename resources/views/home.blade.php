@extends('app')

@section('content')

<!-- Main -->

<main id="main">
    <div id="carousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/IC.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/GI.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/CienciasF.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/Forense.jpg" class="d-block w-100" alt="...">
            </div>

            <div class="overlay">
                <div class="container">
                    <div class="row align-items-center">

                        <div class="col-md-6 offset-md-6 text-md-right text-center">
                            <h1>Criminalística Forense</h1>
                            <p class="d-none d-md-block">
                                Un evento que vale la pena conocer, la primera experiencia con profesionales
                                que llevan el aprendizaje a otro nivel.
                            </p>
                            <a href="#" class="btn btn-outline-light">Regístrarme</a>
                            <button class="btn btn-criminalistica">Más información</button>
                        </div>

                    </div>

                </div>
            </div>



        </div>

    </div>
</main>

<!-- /Main -->

<!-- Cursos -->

<section id="cursos" class="mt-4">
<div class="container">
    <div class="row">
        <div class="col text-center text-uppercase">
            <small>Conoce nuestros</small> <h2>Cursos</h2>
        </div>

    </div>

    <div class="row">

     
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                <img src="images/H1.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Criminología y ciencia forense</h5>
                  <p class="card-text">Está enfocado a que comprendas de qué manera los principios científicos básicos, 
                      apoyan a la ciencia forense y, pueden ayudar a resolver casos criminales.</p>
                      <a href="#" class="btn btn-criminalistica" data-toggle="modal" data-target="#CriminologíaModal">Ver curso</a>
                </div>
              </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                <img src="images/CuroCri.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Introducción a la técnica del perfil criminológico</h5>
                  <p class="card-text"> Se trata de una excelente oportunidad para conocer más acerca del comportamiento y 
                      la relación social de diferentes tipos de perfiles psicológicos. 
                  </p>
                  <a href="#" class="btn btn-criminalistica" data-toggle="modal" data-target="#IntroducciónModal">Ver curso</a>
                </div>
              </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                <img src="images/D.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Introducción a la conducta criminal y psicopatía</h5>
                  <p class="card-text"> Te introducirás al estudio de la conducta criminal y la mente del psicópata para resolver
                       algunos de los interrogantes que nos planteamos cuando alguien comente un crimen.</p>
                       <a href="#" class="btn btn-criminalistica" data-toggle="modal" data-target="#CriminalModal">Ver curso</a>
                </div>
              </div>
        </div>
    </div>
</div>
</section>

<!-- /Cursos -->

<!-- Blog -->
<section id="blog">
<div clas="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-12 pl-0 pr-0">
            <img src="images/blog.jpg" alt="">
        </div>
            <div class="col-lg-6 col-12 pt-4 pb-4">
                <h2> Criminalística Forense 2021 </h2>
                <p> Estudiamos el origen del delito, la conducta del delincuente, la víctima y 
                    creamos políticas de prevención para el control social. 
                    Regístrate a la página, rellena el formulario de contacto para que puedas tener más información 
                    sobre los temas que te agraden y así cubrir tus necesidades acádemicas.
                </p>
                <a href="#" class="btn btn-outline-light">Regístrarme</a>
            </div>
    </div>

</div>
</section>
<!-- /Blog -->

<!-- Tienda -->

<section id="tienda" class="mt-4">
<div class="container">
    <div class="row">
        <div class="col text-center text-uppercase">
            <small>Adquiere nuestros</small> <h2>Libros</h2>
        </div>

    </div>

    <div class="row">

     
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                <img src="images/L1.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Criminología, criminalística e investigación.</h5>
                      <a href="#" class="btn btn-criminalistica" data-toggle="modal" data-target="#InvestigaciónModal">Comprar</a>
                </div>
              </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                <img src="images/L2.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Manual de criminalística moderna.</h5>
                  <a href="#" class="btn btn-criminalistica" data-toggle="modal" data-target="#ManualnModal">Comprar</a>
                </div>
              </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                <img src="images/L3.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Criminología, criminalística y victimología</h5>
                       <a href="#" class="btn btn-criminalistica" data-toggle="modal" data-target="#CriModal">Comprar</a>
                </div>
              </div>
        </div>
    </div>
</div>
</section>

<!-- /Tienda -->
<!-- Regístrate -->
<section id="registrate" class="pt-4 pb-4">
<div class="container">
    <div class="row">
        <div class="col text-center">
            <small class="text-uppercase">Convierte tú conocimiento en </small>
            <h2>Criminalística</h2>
        </div>
    </div>

    <div class="row">
        <div class="col text-center">
            Participa en el proceso de selección y forma parte del equipo <abbr data-toggle="tooltip" title="El nombre de tu nueva familia">Criminalística forense</abbr>.
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 pt-2">
            <form>
                <div class="form-row">
                    <div class="col-12 col-md-6 form-group">
                        <input type="text" class="form-control" placeholder="Nombre">
                    </div>
                    <div class="col-12 col-md-6 form-group">
                        <input type="text" class="form-control" placeholder="Apellidos">
                    </div>
                </div>

                <div class="form-row">
                    <div class="col form-group">
                        <textarea name="descripcion" class="form-control"></textarea>
                        <small>Incluye un título en tu descripción</small>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col form-group">
                        <button type="button" class="btn btn-criminalistica btn-block">Envíar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- /Regístrate -->

@endsection