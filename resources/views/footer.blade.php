<!--FOOTER-->
<footer id="footer" class="pb-4 pt-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg">
                <div class="row">
                    <div class="col-12 col-lg">

                        <div class="row">
                            <div class="col-12 col-lg-12 col-med-12">
                                <h5>Contacto</h5>
                            </div>

                            <div class="col-12 col-lg-12 col-med-12">
                                <small>16 de septiembre #20</small>
                            </div>
                            <div class="col-12 col-lg-12 col-med-12">
                                <small>Tel: 47 41 26 69 68</small>
                            </div>
                        </div>

                    </div>

                    <div class="col-12 col-lg">
                        <a href="#">Preguntas frecuentes</a>
                        <p><small>¿Porqué es importante la criminalística forense?</p></small>
                        <p><small>¿Es relevante cada tema?</p></small>
                        <p><small>¿Es de suma importante estudiar la carrera?</p></small>

                    </div>

                    <div class="col-12 col-lg">
                        <a href="#">Términos y condiciones</a>
                        <p><small>ACEPTACIÓN DE LAS CONDICIONES DE USO.</small></p>
                        <p><small>PROMETE O JURA.</small></p>
                        <p><small>INFORMAMOS.</small></p>
                        <p><small>DECLARAMOS.</small></p>
                    </div>
                </div>
            </div>
</footer>
<div id="footerDivider" class="pt-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12">
                <p>Copyright @2021 Criminalística Forense</p>
            </div>
            <div class="col-lg-6 col-12 text-lg-right text-left">
                <a href="#">Transparencia</a>
            </div>

        </div>

    </div>
</div>

<!--END FOOTER /FOOTER-->