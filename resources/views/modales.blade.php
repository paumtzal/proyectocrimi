<!-- Modal -->
<div class="modal fade" id="IntroducciónModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Contenido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Esquema de conceptos básicos.
                        <span class="badge badge-primary badge-pill">2 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Orígenes.
                        <span class="badge badge-primary badge-pill">3 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Elementos importantes para la realización de un perfil.
                        <span class="badge badge-primary badge-pill">5 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Asesinos múltiples.
                        <span class="badge badge-primary badge-pill">2 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Tipología del violador.
                        <span class="badge badge-primary badge-pill">3 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Anexos.
                        <span class="badge badge-primary badge-pill">2 hrs.</span>
                    </li>
                </ul>

                <div class="alert alert-success" role="alert">
                    17 horas de curso.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="CriminologíaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Contenido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Introducción a la ciencia forense.
                        <span class="badge badge-primary badge-pill">4 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Análisis químico en ciencias forenses.
                        <span class="badge badge-primary badge-pill">3 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Hora de la muerte: sangre.
                        <span class="badge badge-primary badge-pill">3 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        ADN en forense.
                        <span class="badge badge-primary badge-pill">4 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Toma de huellas dáctilares, polímeros / fibras y armas de fuego.
                        <span class="badge badge-primary badge-pill">5 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Narcóticos.
                        <span class="badge badge-primary badge-pill">2 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Toxicología.
                        <span class="badge badge-primary badge-pill">2 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Estudios de casos.
                        <span class="badge badge-primary badge-pill">2 hrs.</span>
                    </li>
                </ul>

                <div class="alert alert-success" role="alert">
                    25 horas de curso.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="CriminalModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Contenido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Introducción.
                        <span class="badge badge-primary badge-pill">8 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Conceptos de psicopatía.
                        <span class="badge badge-primary badge-pill">3 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Aspectos neurológicos relacionados con la psicopatía.
                        <span class="badge badge-primary badge-pill">3 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Personalidad y violencia.
                        <span class="badge badge-primary badge-pill">4 hrs.</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Conducta criminal.
                        <span class="badge badge-primary badge-pill">5 hrs.</span>
                    </li>

                <div class="alert alert-success" role="alert">
                    23 horas de curso.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>